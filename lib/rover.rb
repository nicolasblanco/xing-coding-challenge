class Rover
  HEADINGS = %w(N E S W)
  attr_accessor :x, :y
  attr_reader :heading

  def initialize(x:, y:, heading:)
    @x, @y = x, y
    self.heading = heading
  end

  def heading=(new_heading)
    raise InvalidHeading unless HEADINGS.include?(new_heading)

    @heading = new_heading
  end

  def turn_left!
    @heading = HEADINGS.at(HEADINGS.rotate(1).index(@heading))
  end

  def turn_right!
    @heading = HEADINGS.at(HEADINGS.rotate(-1).index(@heading))
  end

  def move_forward!
    case heading
    when 'N' then self.y += 1
    when 'E' then self.x += 1
    when 'S' then self.y -= 1
    when 'W' then self.x -= 1
    end
  end

  def to_s
    "#{x} #{y} #{heading}"
  end

  class InvalidHeading < StandardError; end
end
