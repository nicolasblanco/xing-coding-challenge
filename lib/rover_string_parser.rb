class RoverStringParser
  ROVER_COMMANDS_MAPPING = { 'L': :turn_left!, 'R': :turn_right!, 'M': :move_forward! }

  def self.rover_from_string(str)
    rover_data = str.split(' ')
    Rover.new(x: rover_data[0].to_i, y: rover_data[1].to_i, heading: rover_data[2])
  end

  def self.rover_commands_from_string(str)
    str.split('').map { |letter| rover_command_from_letter(letter) }
  end

  def self.execute_commands!(commands_string:, rover:)
    rover_commands_from_string(commands_string).each do |command|
      rover.send(command)
    end
  end

  class InvalidCommand < StandardError; end

  private

  def self.rover_command_from_letter(chr)
    ROVER_COMMANDS_MAPPING[chr.to_sym] ||
      (raise InvalidCommand, "cannot find command mapping for letter '#{chr}'")
  end
end
