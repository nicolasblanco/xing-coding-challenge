class Grid
  attr_accessor :rovers, :size_x, :size_y

  def initialize(size_x:, size_y:)
    @rovers = []
    @size_x, @size_y = size_x, size_y
  end

  def self.new_from_string(str)
    size_data = str.split(' ')
    new(size_x: size_data[0].to_i, size_y: size_data[1].to_i)
  end
end
