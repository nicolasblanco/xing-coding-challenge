XING Coding Challenge
=====================

Hi! This is my solution for the Rovers on Mars coding challenge.

The solution to the challenge is provided entirely using Ruby specs.

Every class is unit tested in the `spec` folder.

Integration testing using the data provided in the challenge is found at `spec/integration_spec.rb`.

All the specs should be self explanatory, no documentation is provided :).

Requirements
------------

* Ruby 2.3.1
* Bundler 1.12.5

Installation
------------

    bundle
    bin/rspec # To run all the specs, should be always green!
