require 'rover_string_parser'

RSpec.describe RoverStringParser do
  describe '.rover_from_string' do
    let(:rover) { described_class.rover_from_string('1 2 N') }

    it 'returns a new Rover using a string' do
      expect(rover.x).to eq(1)
      expect(rover.y).to eq(2)
      expect(rover.heading).to eq('N')
    end
  end

  describe '.rover_commands_from_string' do
    context 'when using a valid commands line' do
      let(:commands_string) { 'LMR' }
      let(:rover_commands) { %i(turn_left! move_forward! turn_right!) }

      it 'returns a list of commands for the rover' do
        expect(described_class.rover_commands_from_string(commands_string)).to eq(rover_commands)
      end
    end

    context 'when using an invalid commands line' do
      let(:commands_string) { 'LMRA' }

      it 'raises an exception' do
        expect { described_class.rover_commands_from_string(commands_string) }.to raise_error(RoverStringParser::InvalidCommand)
      end
    end
  end

  describe '.execute_commands!' do
    let(:commands_string) { 'LMR' }
    let(:rover) { double('rover') }

    it 'executes commands on the given rover' do
      expect(rover).to receive(:turn_left!).once
      expect(rover).to receive(:move_forward!).once
      expect(rover).to receive(:turn_right!).once

      described_class.execute_commands! commands_string: commands_string, rover: rover
    end
  end
end
