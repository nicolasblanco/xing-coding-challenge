require 'grid'

RSpec.describe Grid do
  context 'after initialization' do
    let(:grid) { described_class.new(size_x: 5, size_y: 5) }

    it 'has not rovers' do
      expect(grid.rovers).to be_empty
    end
  end

  describe '.new_from_string' do
    let(:grid) { described_class.new_from_string('10 15') }
    it 'returns a new grid from a string' do
      expect(grid.size_x).to eq(10)
      expect(grid.size_y).to eq(15)
    end
  end
end
