require 'grid'
require 'rover'
require 'rover_string_parser'

RSpec.describe 'Integration' do
  let(:grid_initialization) { '5 5' }

  let(:rover_1_initial_position) { '1 2 N' }
  let(:rover_1_commands) { 'LMLMLMLMM' }

  let(:rover_2_initial_position) { '3 3 E' }
  let(:rover_2_commands) { 'MMRMMRMRRM' }

  let(:grid) { Grid.new_from_string(grid_initialization) }

  context 'first rover' do
    it 'moves the rover according to the first sequence' do
      rover = RoverStringParser.rover_from_string(rover_1_initial_position)
      grid.rovers << rover

      RoverStringParser.execute_commands!(commands_string: rover_1_commands, rover: rover)

      expect(rover.to_s).to eq('1 3 N')
    end
  end

  context 'second rover' do
    it 'moves the rover according to the second sequence' do
      rover = RoverStringParser.rover_from_string(rover_2_initial_position)
      grid.rovers << rover

      RoverStringParser.execute_commands!(commands_string: rover_2_commands, rover: rover)

      expect(rover.to_s).to eq('5 1 E')
    end
  end
end
