require 'rover'

RSpec.describe Rover do
  let(:rover) { Rover.new(x: 0, y: 0, heading: 'N') }

  describe '#heading=' do
    context 'when heading is valid' do
      it 'sets the new heading' do
        rover.heading = 'S'

        expect(rover.heading).to eq('S')
      end
    end

    context 'when heading is invalid' do
      it 'raises an exception' do
        expect { rover.heading = 'P' }.to raise_error(Rover::InvalidHeading)
      end
    end
  end

  describe '#turn_left!' do
    it 'sets the current heading 90 degrees to the left' do
      rover.turn_left!

      expect(rover.heading).to eq('W')
    end
  end

  describe '#turn_right!' do
    it 'sets the current heading 90 degrees to the right' do
      rover.turn_right!

      expect(rover.heading).to eq('E')
    end
  end

  describe '#to_s' do
    let(:rover) { Rover.new(x: 2, y: 3, heading: 'S') }
    it 'returns the rover as a formatted string' do
      expect(rover.to_s).to eq('2 3 S')
    end
  end

  describe '#move_forward!' do
    context 'when current heading is North' do
      before(:each) { rover.heading = 'N' }

      it 'increments y by 1 and does not change x and heading' do
        rover.move_forward!

        expect(rover.y).to eq(1)
        expect(rover.x).to eq(0)
        expect(rover.heading).to eq('N')
      end
    end

    context 'when current heading is East' do
      before(:each) { rover.heading = 'E' }

      it 'increments x by 1 and does not change y and heading' do
        rover.move_forward!

        expect(rover.x).to eq(1)
        expect(rover.y).to eq(0)
        expect(rover.heading).to eq('E')
      end
    end

    context 'when current heading is South' do
      before(:each) { rover.heading = 'S' }

      it 'decrements y by 1 and does not change x and heading' do
        rover.move_forward!

        expect(rover.y).to eq(-1)
        expect(rover.x).to eq(0)
        expect(rover.heading).to eq('S')
      end
    end

    context 'when current heading is West' do
      before(:each) { rover.heading = 'W' }

      it 'decrements x by 1 and does not change x and heading' do
        rover.move_forward!

        expect(rover.x).to eq(-1)
        expect(rover.y).to eq(0)
        expect(rover.heading).to eq('W')
      end
    end
  end
end
